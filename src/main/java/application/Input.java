package application;

import java.util.ArrayList;
import java.util.Scanner;

public class Input {

  public static String scan() {
    Scanner scanner = new Scanner(System.in);
    String s = "";
    try {
      if (scanner.hasNext()) {
        s = scanner.nextLine();
      }

    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
    return s;
  }

  public static ArrayList<String> inputFromUser() {

    System.out.println("to quit enter \"q\"");
    System.out.println("enter any lines: ");

    ArrayList<String> list = new ArrayList<>();
    String s = scan();
    while (!s.equals("q")) {
      list.add(s);
      s = scan();
    }
    return list;

  }


}
