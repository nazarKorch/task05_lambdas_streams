package application;

import java.util.ArrayList;

public class MainApp {

  public static void main(String[] args) {
    ArrayList<String> list = Input.inputFromUser();
    System.out.println("Occurrence number of each symbol:");
    System.out.println(Application.getSymbolCount(list));
    System.out.println("Number of unique words: ");
    System.out.println(Application.getNumberOfUnique(list));
    System.out.println("Sorted list of all unique words:");
    System.out.println(Application.getUniqueList(list));
    System.out.println("Occurrence number of each word:");
    System.out.println(Application.getWordCount(list));

  }

}
