package application;

import java.util.ArrayList;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Application {

  public static long getNumberOfUnique(ArrayList<String> arrayList) {
    return arrayList.stream()
        .flatMap(n -> Stream.of(n.split(" ")))
        .distinct()
        .count();
  }

  public static ArrayList<String> getUniqueList(ArrayList<String> arrayList) {

    return (ArrayList<String>) arrayList.stream()
        .flatMap(n -> Stream.of(n.split(" ")))
        .distinct()
        .sorted()
        .collect(Collectors.toList());

  }


  public static Map<String, Long> getWordCount(ArrayList<String> arrayList) {

    return arrayList.stream()
        .flatMap(n -> Stream.of(n.split(" ")))
        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
  }

  public static Stream<String> splitWords(String str) {
    Stream<String> stream = Stream.of(str.split("(?!^)"));
    return stream;
  }

  public static Map<String, Long> getSymbolCount(ArrayList<String> arrayListr) {

    return arrayListr.stream()
        .flatMap(n -> Stream.of(n.split(" ")))
        .flatMap(Application::splitWords)
        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
  }


}
