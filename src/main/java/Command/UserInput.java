package Command;

import java.util.Scanner;

public class UserInput implements Command {

  public static void getFirst(String s) {
    Command command = str -> System.out.println("first command invoked, your input: " + str);
    command.callCommand(s);
  }

  private static void print(String str) {
    System.out.println("second command invoked, your input: " + str);

  }

  public static void getSecond(String s) {
    Command command = (UserInput::print);
    command.callCommand(s);
  }


  public static void getThird(String s) {
    Command command = new Command() {
      @Override
      public void callCommand(String str) {
        System.out.println("third command invoked, your input: " + str);
      }
    };
    command.callCommand(s);
  }

  public void callCommand(String str) {
    System.out.println("fourth command invoked, your input: " + str);
  }

  public static void getFourth(String s) {
    Command command = new UserInput();
    command.callCommand(s);
  }


  public static void input() {
    String name = " ";
    Scanner sc = new Scanner(System.in);

    try {
      System.out.println("enter name of command (first, second, third or fourth):");
      if (sc.hasNext()) {
        name = sc.nextLine();
      } else {
        input();
      }

        switch (name) {
          case "first":
            System.out.println("enter your string:");
            getFirst(sc.nextLine());
            break;
          case "second":
            System.out.println("enter your string:");
            getSecond(sc.nextLine());
            break;
          case "third":
            System.out.println("enter your string:");
            getThird(sc.nextLine());
            break;
          case "fourth":
            System.out.println("enter your string:");
            getFourth(sc.nextLine());
            break;
          default:
            System.out.println("wrong name of command!");
            input();
            break;
        }


    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

}
