package one;

import static java.util.Arrays.stream;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Stream;

public class Main {

  public static void main(String[] args) {
    Tasker max = (a, b, c) -> {
      int maxN = Stream.of(a, b, c)
          .max(Comparator.comparing(Integer::valueOf))
          .orElse(-1);
      return maxN;
    };
    Tasker avg = (a, b, c) -> {
      int [] avgN = {a, b, c};
      Arrays.sort(avgN);
      return avgN[1];
    };

    System.out.println(max.functMethod(4,2,9));
    System.out.println(avg.functMethod(8,100,-2));
  }

}
