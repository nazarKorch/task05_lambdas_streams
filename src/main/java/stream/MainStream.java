package stream;

import java.util.ArrayList;
import java.util.Random;
import java.util.stream.Collectors;

public class MainStream {

  public static int getMax(ArrayList<Integer> arrayList) {
    return arrayList.stream()
        .mapToInt(Integer::valueOf)
        .max()
        .orElse(-1);
  }

  public static int getMin(ArrayList<Integer> arrayList) {
    return arrayList.stream()
        .mapToInt(Integer::valueOf)
        .min()
        .orElse(-1);
  }

  public static int getSum(ArrayList<Integer> arrayList) {
    return arrayList.stream()
        .mapToInt(Integer::valueOf)
        .reduce((a, b) -> a + b)
        .orElse(-1);
  }

  public static ArrayList<Integer> getList() {
    Random r = new Random();
    ArrayList<Integer> arrayList = new ArrayList<>();
    for (int i = 0; i < r.nextInt(8) + 2; i++) {
      arrayList.add(r.nextInt(20));
    }
    return arrayList;

  }

  public static ArrayList<Integer> getNumbersBiggerAvg(ArrayList<Integer> arrayList) {
    int avg = (int) arrayList.stream()
        .mapToInt(n -> n)
        .average()
        .orElse(-1);

    return (ArrayList<Integer>) arrayList.stream()
        .filter(n -> n > avg)
        .collect(Collectors.toList());
  }

  public static void main(String[] args) {
    Streamer streamer = (MainStream::getList);
    ArrayList<Integer> list = streamer.getRandomList();
    System.out.println(list);
    System.out.println("max = " + getMax(list));
    System.out.println("min = " + getMin(list));
    System.out.println("sum = " + getSum(list));
    System.out.println("numders = " + getNumbersBiggerAvg(list));


  }

}
